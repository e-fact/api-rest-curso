package com.curso.spring.api.repositories;

import com.curso.spring.api.models.Categoria;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class CategoriaRepositoryTest {
    
    @Autowired
    CategoriaRepository categoriaRepository;

    @Test
    public void save(){
        
        Categoria categoria = new Categoria();
        categoria.setDescripcion("VERDURAS");
        categoria.setId(4);
        categoria = categoriaRepository.save(categoria);
        Assert.assertEquals(categoria, categoriaRepository.findById(4).get());

        

    }



}