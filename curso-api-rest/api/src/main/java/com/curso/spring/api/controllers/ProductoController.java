package com.curso.spring.api.controllers;

import java.util.List;

import com.curso.spring.api.models.Producto;
import com.curso.spring.api.repositories.ProductoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductoController {
    
    @Autowired
    private ProductoRepository productoRepository;


    @GetMapping("/productos")
    public List<Producto> getproducto(){
         List<Producto> productos = productoRepository.findAll();
         return  productos;      
    }


    @PostMapping("/productos")
    public Producto postproducto(@RequestBody Producto producto){
        return productoRepository.save(producto);
    }



}