package com.curso.spring.api;


import com.curso.spring.api.models.Producto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
		
		Producto producto = new Producto();
		producto.setDescripcion("Lapiz");
		producto.setPrecio(10);
		producto.setStock(30);

		System.out.println(producto);

		

	}

}
