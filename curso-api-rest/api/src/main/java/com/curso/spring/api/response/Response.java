package com.curso.spring.api.response;

import java.util.ArrayList;
import java.util.List;


public class Response<T> {

    private T result;
    private List<String> errors;


    public Response(){

    }

    public T getResult (){
        return this.result;
    }

    public List<String> getErrors (){
        if (errors == null){
            this.errors = new ArrayList<>();
        }
        return errors;
    }

    public void setResult(T result){
        this.result=result;
    }

    public void setErrors(List<String> errors){
        this.errors = errors;
    }


    
}