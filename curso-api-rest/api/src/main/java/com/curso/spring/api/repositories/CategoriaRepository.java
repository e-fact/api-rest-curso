package com.curso.spring.api.repositories;
import com.curso.spring.api.models.Categoria;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CategoriaRepository extends JpaRepository<Categoria,Integer>{

    Categoria findByDescripcion(String descripcion);

}